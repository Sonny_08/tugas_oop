<?php

class Ape extends Animal{
  public function getName(){
    return "Nama kera : " . $this->name;
  }
  public function getLegs(){
    return "Banyak kaki : " . $this->legs;
  }
  public function getCold_blood(){
    return "Hewan berdarah dingin : " . $this->cold_blood;
  }

  public function Yell(){
    return "Auooooo";
  }

}
?>
