<?php

class Frog extends Animal{
  public function getName(){
    return "Nama kodok : " . $this->name;
  }
  public function getLegs(){
    return "Banyak kaki : " . $this->legs;
  }
  public function getCold_blood(){
    return "Hewan berdarah dingin : " . $this->cold_blood;
  }

  public function Jump(){
    return "Hop-hop";
  }

}
?>
