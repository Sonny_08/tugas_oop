<?php
class Animal{

  public $name, $legs = 2, $cold_blood ="false";
  public function __constructor($name , $legs = 2, $cold_blood="false"){
    $this->name = $name;
    $this->legs = $legs;
    $this->cold_blood = $cold_blood;
  }
  public function getName(){
    return "Nama hewan : " . $this->name;
  }
  public function getLegs(){
    return "Banyak kaki : " . $this->legs;
  }
  public function getCold_blood(){
    return "Hewan berdarah dingin : " . $this->cold_blood;
  }

}



?>
