<?php
require_once 'Animal.php';
require_once 'Frog.php';
require_once 'Ape.php';

echo "SOAL RELEASE 1";
echo "<br>";
$sheep = new Animal;
$sheep->name ="shaun";
echo $sheep->getName();
echo "<br>";
echo $sheep->getLegs();
echo "<br>";
echo $sheep->getCold_blood();
echo "<br><br>";

echo "SOAL RELEASE 2";
echo "<br>";

$kodok = new Frog;
$kodok->name = "buduk";
$kodok->legs = 4;
echo $kodok->getName();
echo "<br>";
echo $kodok->getLegs();
echo "<br>";
echo $kodok->getCold_blood();
echo "<br>";
echo $kodok->jump();
echo "<br><br>";

$kodok = new Ape;
$kodok->name = "kera sakti";
echo $kodok->getName();
echo "<br>";
echo $kodok->getLegs();
echo "<br>";
echo $kodok->getCold_blood();
echo "<br>";
echo $kodok->Yell();


?>
